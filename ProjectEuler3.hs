main = do
let primes = 2 : filter (null . tail . primeFactors) [3,5..]
 
let primeFactors n = factor n primes
  where
    factor n (p:ps) 
        | p*p > n        = [n]
        | n `mod` p == 0 = p : factor (n `div` p) (p:ps)
        | otherwise      =     factor n ps
 
let problem_3 = last (primeFactors 600851475143)

print(problem_3)
