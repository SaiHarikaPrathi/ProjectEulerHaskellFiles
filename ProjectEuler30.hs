import Data.Char (digitToInt)

limit :: Integer
limit = snd $ head $ dropWhile (\(a,b) -> a > b) $ zip (map (9^5*) [1..]) (map (10^) [1..])

fifth :: Integer -> Integer
fifth = sum . map ((^5) . toInteger . digitToInt) . show
