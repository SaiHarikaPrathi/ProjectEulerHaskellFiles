main = do
let problem_1  = sum [x | x <- [1..999], rem x 3 == 0 || rem x 5 == 0]
print(problem_1)
