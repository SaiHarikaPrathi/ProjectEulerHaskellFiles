import Data.Char
main = do

let factorial n = if n < 2 then 1 else n * factorial(n - 1)
let fact = factorial 100
let s = show(fact)
let k = map digitToInt s
let problem_20 = sum k

print(problem_20)
