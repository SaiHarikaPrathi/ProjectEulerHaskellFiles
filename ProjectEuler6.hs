main = do
let sumOf100NaturalNumbers = sum[1..100]
let squareOfSumOf100NaturalNumbers = sumOf100NaturalNumbers^2
let sumOfSquaresOf100NaturalNumbers = sum(map(^2)[1..100])
let diff = squareOfSumOf100NaturalNumbers - sumOfSquaresOf100NaturalNumbers
print(diff)
