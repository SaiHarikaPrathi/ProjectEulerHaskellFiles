main = do
let primes = sieve [2..] where sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]
let limit = sum[x |x <- takeWhile(< 2000000) primes]
print(limit)
